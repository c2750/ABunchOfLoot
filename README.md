# A bunch of loot

> ## Goal
> 
> With this mod you will have more reason to explore the game. You can find new structures, new items and new mobs (soon)

## Features

### Items

- Viper's sword : gives poison to target
- Frost sword : traps the target in powder snow
- Blackfrost sword : like frost sword but with more powder snow and deals more damage
- Frost shard : used to craft frost sword

### Blocks

- Healer : a block that heals you
- Packed ice brick & variants : a decoration block
- Blue ice brick & variants : also a decoration block

### Structures

- Jungle well : found in jungle, you might find some loot at the bottom
- Graves : you can find a few variants around in the world, there are also a little bit of loot
- Ice crypts : a large structure made of ice bricks, this structure is generated in cold biome

## Versions

It currently supports minecraft 1.18.1 with Forge. 

- I wont backport the mod
- But I will probably port it to future versions

## Links

> You can download the mod on curseforge : https://www.curseforge.com/minecraft/mc-mods/a-bunch-of-loot
